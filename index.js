var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var methodOverride = require('method-override');
var app = express();

mongoose.connect('mongodb://localhost/Aleno', { useNewUrlParser: true }, function(err, res) {
    if (err) throw err;
    console.log('Connected to Database');
});

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride());

var movementModel = require('./models/movements')(mongoose);

var MovementCtrl = require('./controllers/movement');

var router = express.Router();

router.get('/', function(req, res) {
    res.send("Server is listening to you!");
});
app.use(router);

var api = express.Router();
api.route('/movements')
    .get(MovementCtrl.findAll)
    .post(MovementCtrl.add)
    .put(MovementCtrl.update)
    .delete(MovementCtrl.delete);

api.route('/movements/:email')
    .get(MovementCtrl.findByMail);

app.use('/api', api);

app.listen(3000, function() {
    console.log("Node server running on http://localhost:3000");
});