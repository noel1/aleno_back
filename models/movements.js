var mongoose = require('mongoose');
var Schema = mongoose.Schema;
//creo el esquema para los movimientos 
var movementSchema = new Schema({
    email: String,
    date: {
        type: Date,
        default: Date.now
    },
    type: {
        type: String,
        enum: ['Compra', 'Rechazo', 'Cancelacion', 'Actualizacion', 'Alta']
    },
    cellPhone: Number,
    compania: String,
    quantity: Number,
}, {
    toObject: {
        virtuals: true
    },
    toJSON: {
        virtuals: true
    }
});

module.exports = mongoose.model('Movement', movementSchema);