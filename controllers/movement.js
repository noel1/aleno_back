var mongoose = require('mongoose');
var Movement = mongoose.model('Movement');
//all movements
exports.findAll = function(req, res) {
    Movement.find(function(err, movements) {
        if (err)
            res.status(500).send(err.message);
        console.log('GET/movements');
        res.status(200).jsonp(movements);
    });
};
//movements by client
exports.findByMail = function(req, res) {
    Movement
        .find({ email: req.params.email })
        .exec(function(err, client) {
            if (err)
                return res.status(500).send(err.message);
            console.log('GET/clients/' + req.params.email);
            res.status(200).jsonp(client);
        });
};
//add a movement
exports.add = function(req, res) {
    console.log('POST/movements');
    var movement = new Movement({
        email: req.body.email,
        type: req.body.type,
        quantity: req.body.quantity,
        cellPhone: req.body.cellPhone,
        compania: req.body.compania
    });
    console.log('datos:', movement);
    movement.save(function(err) {
        if (err)
            return res.status(500).send(err.message);
        res.status(200).json(movement);
    });
};
//update a movement
exports.update = function(req, res) {
    Movement.findById(req.body.id, function(err, movement) {
        movement.email = req.body.email ? req.body.email : movement.email;
        movement.type = req.body.type ? req.body.type : movement.type;
        movement.quantity = req.body.quantity ? req.body.quantity : movement.quantity;
        movement.cellPhone = req.body.cellPhone ? req.body.cellPhone : movement.cellPhone;
        movement.compania = req.body.compania ? req.body.compania : movement.compania;

        console.log('PUT/movements/' + req.body.id);
        console.log(movement);
        movement.save(function(err) {
            if (err)
                return res.status(500).send(err.message);
            res.status(200).jsonp(movement);
        });
    });
};

//delete a movement
exports.delete = function(req, res) {
    Movement.findById(req.body.id, function(err, movement) {
        console.log('DELETE/movements/' + req.body.id);
        console.log(movement);
        movement.remove(function(err) {
            if (err)
                return res.status(500).send(err.message);
            res.json({ message: 'Successfully deleted' });
        });
    });
};